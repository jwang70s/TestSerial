# TestSerial

#### Description

**Test equipment by send/respond data via serial port and show timing volts notes**

Send command data and get respond data from the equipment and compare the responded data with expect value to varify the serial communication.

The command/respond data will be shown in timing voltages notes which can be compared with the wave getting from a oscilloscope on the bus.

An offline model without real command sending is also available to show timing voltage notes for the command only.

    Shown:
        OnLine model: Serial 'rs485' is opened at port:'/dev/cu.usbserial-AL0617PC'
        Command(Enter for last):7 166 10
        Expected Respond:7 2 10
        Will be send: 7 166 10 
        Timing Volts: _---_____-|__--__-_--|__-_-____-|
        Expected Respond: 7 2 10 
        Timing Volts: _---_____-|__-______-|__-_-____-|
        Serial Recieve: 7 2 10 , Meet the expected.

```Written by J.wang(Wang Jian)```

```version: 0.1.0```
 
Source opened at : https://gitee.com/jwang70s/TestSerial with license of Apache 2.0

jwang70s@sina.com

#### Software Architecture
'testSerial.py'

'README.md'

'README.en.md'


#### Installation

Direct running from console

#### Instructions

1. Run

    `python3 testSerial.py`

    >Optional arguments:

        -h, --help            show this help message and exit
        -p PORTNAME, --portname PORTNAME
                                serial port name for connection,none for offline
        -s SERIALTYPE, --serialtype SERIALTYPE
                                type name from 'RS485','RS422','RS232','TTL'(default)
    >Example:

        python3 testSerial.py -p com5 -s TTL
2. Input

    When running, the console will show a online/offline and serial type prompt.
    Then a command line is shown:'Command(Enter for last):'

    After input command a Respond line will be shown(only online model): 'Expected Respond'

    The available input notes are as follows:

    >Command line: 

        Input command to be send vai serial port.
            Only show timing vlolts notes without real send in Offline model.
            Press Enter (without input) will use the last input value of BOTH Command and Respond.
    >Respond line:
    
    Input expected respond data (including endings)
        Not available in Offline model.

        Input Example:
            (hex input)
                Command(Enter for last):0x680x740x68
                Command(Enter for last):0x687468
                Command(Enter for last):0x68 74   68,68
            (int input)
                Command(Enter for last):104,116  104
               
        Outputs are like:
            Command(Enter for last):0xaabb
            Will be send: 170 187
            Timing Volts: __-_-_-_--|_--_---_--|
            The programe will automatic transfer the hex value to int value for shown.

            '-' present High voltage while
            '_' present Low voltage
            '|' present a seperator between different value(will not appear in real voltage wave)
            These three notes and be set to other char in config file.
                 Seperator '|'can be delete as an define of '' in config file
                 High/Low can not be deleted

    >Quit:  

        Type 'quit' in command line for quit.  

3. Options

    ```Save the configration to file (yes/no(Enter))?```
            will prompt for asking saving configuration to file when quiting.

    Type 'yes' and a configration file will be found in the same directory as a name of 'config.json',
    in which, more options can be set like:
    ```byte_size,parity,stop_bits,low_volt_note,high_volt_note,seperator_note ETC.```

    Note: note_times is better set to 2 if stop_bit is 1.5 for a precise sake.

        Taken a votage note for number 2 in TTL:
        note_times = 1   __-______-|
        note_times = 2   ____--____________--|

#### Contribution

1. J.Wang(WangJian)
    jwang70s@sina.com
    GITEE:  https://gitee.com/jwang70s


#### Gitee Feature

1. You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2. Gitee blog [blog.gitee.com](https://blog.gitee.com)
3. Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4. The most valuable open source project [GVP](https://gitee.com/gvp)
5. The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6. The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)