# TestSerial

#### 介绍
**串口设备通讯校验与时序图绘制**

发送(本机)串口指令数据，接收设备(从机)回复，并比对设备回复与期待回复是否相同以验证串口通讯是否有效。

同时在控制台输出发送指令数据与期待回复数据的电位时序波型，可以用来与示波器显示的波型进行比对。

在离线状态可以仅用于显示数据波型。

    运行效果：
        OnLine model: Serial 'rs485' is opened at port:'/dev/cu.usbserial-AL0617PC'
        Command(Enter for last):7 166 10
        Expected Respond:7 2 10
        Will be send: 7 166 10 
        Timing Volts: _---_____-|__--__-_--|__-_-____-|
        Expected Respond: 7 2 10 
        Timing Volts: _---_____-|__-______-|__-_-____-|
        Serial Recieve: 7 2 10 , Meet the expected.

```程序:J.wang(王建)```

```版本: 0.1.0```
 
原代码: https://gitee.com/jwang70s/TestSerial 

协议:   Apache 2.0

jwang70s@sina.com


#### 软件架构

'testSerial.py'

'README.md'

'README.en.md'


#### 安装教程

在控制台中运行

#### 使用说明

1. 运行

    `python3 testSerial.py`

    >可选参数:

        -h, --help            show this help message and exit 帮助
        -p PORTNAME, --portname PORTNAME
                                serial port name for connection,none for offline
                                串口识别字串
        -s SERIALTYPE, --serialtype SERIALTYPE
                                type name from 'RS485','RS422','RS232','TTL'(default)
                                串口类型
    >运行实例:

        python3 testSerial.py -p com5 -s TTL
    注意：程序运行时的提示及帮助均为英文以提高通用性

2. 输入

    
    运行后,控制台会显示online/offline状态
    然后会出现命令提示: 'Command(Enter for last):'

    输入发送的命令代码后,会出现期待回复的命令提示: 'Expected Respond:'

    输入时按照下列规则:

    >Command line: 

        输入准备通过串口发送的命令.
            离线模式下仅仅显示电位时序波型
            直接按'Enter'则发送上一次的命令
            
    >Respond line:

        输入期待的回复数据(要包含结尾字符如\n(0x0a/10))
        离线模式下不会出现此命令行

        输入实例:
            (16进制)
                Command(Enter for last):0x680x740x68
                Command(Enter for last):0x687468
                Command(Enter for last):0x68 74   68,68
            (10进制)
                Command(Enter for last):104,116  104
        输出显示类似于:
            Command(Enter for last):0xaabb
            Will be send: 170 187
            Timing Volts: __-_-_-_--|_--_---_--|
            注意:无论输入的是16进制还是10进制,程序会自动统一转换成10进制进行显示
            '-'代表高电位
            '_'代表低电位
            '|'代表字符间的分隔(仅提示作用,实际波形中没有,可通过配制进行关闭)
            这三个符号都可以在配置文件中更改为其它符号,分隔符可以通过''关闭,高低电位不可关闭
    >退出:  

        在发送命令提示符下输入:quit

3. 可配置选项

    在退出时,会出现下面提示

    ```Save the configration to file (yes/no(Enter))?```

    输入 'yes' 会将当前的设置保存至'config.json',通过修改这个文件,可以进行更多的配置 
    ```byte_size,parity,stop_bits,low_volt_note,high_volt_note,seperator_note ETC.```

    注意: 当停止位stop_bit是1.5 时note_times最好设置为2,可以显示地更为精确

        不同的note_times对于电位时序图的影响是:
        note_times = 1   __-______-|
        note_times = 2   ____--____________--|

#### 参与贡献

1. J.Wang(王建)
    jwang70s@sina.com
    GITEE:  https://gitee.com/jwang70s



#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)